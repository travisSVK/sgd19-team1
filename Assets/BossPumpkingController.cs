﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPumpkingController : MonoBehaviour
{
    public int hp = 20;
    public float attackSpeed = 0.5f;
    public float playerAttackDistance = 0.5f;

    private Animator animator;
    private bool attackStarted = false;
    private Vector3 playerPosition;
    private int maxHp;
    
    void OnEnable()
    {
        Transform renderedObj = transform.GetChild(0);
        renderedObj.GetComponent<Renderer>().enabled = true;
        hp = maxHp;
        attackStarted = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerPosition = player.transform.position;

        maxHp = hp;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float step = attackSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, playerPosition, step);
        if ((transform.position.z - playerPosition.z) < 0.001f)
        {
            gameObject.SetActive(false);
        }

        // Attack if close to player
        if (((transform.position.z - playerPosition.z) <= playerAttackDistance) && !attackStarted)
        {
            attackStarted = true;
            Attack();
        }
    }

    private void Attack()
    {
        if (animator)
        {
            animator.SetTrigger("Attack");
        }
    }

    public void AttackFinished()
    {
       // gameObject.SetActive(false);
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            StartCoroutine(Die());
        }
        else
        {
            StartCoroutine(Flash());
        }
    }

    private IEnumerator Die()
    {
        animator.SetBool("Die", true);
        yield return new WaitForSecondsRealtime(animator.GetCurrentAnimatorStateInfo(0).length + 0.02f);
        animator.SetBool("Die", false);

        Transform renderedObj = transform.GetChild(0);
        renderedObj.GetComponent<Renderer>().enabled = false;
        transform.Find("DeathAnim").GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(transform.Find("DeathAnim").GetComponent<ParticleSystem>().main.duration);
        gameObject.SetActive(false);
    }

    private IEnumerator Flash()
    {
        Renderer renderer = transform.GetChild(0).GetComponent<Renderer>();
        Material mat = renderer.material;
        Color oldColor = mat.GetColor("_EmissiveColor");

        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = new Color(0.245283f, 0f, 0f);
        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissiveColor", finalColor);

        yield return new WaitForSeconds(0.2f);

        mat.SetColor("_EmissiveColor", oldColor);
    }
}
