﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingObstacle : MonoBehaviour
{
    public float topBoundary = 0.1f;
    public float bottomBoundary = 0.05f;
    public float floatStep = 0.001f;
    public int collisionDamage = 10;

    private bool upDirection = true;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (upDirection)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + floatStep, transform.position.z);
            if (transform.position.y >= topBoundary)
            {
                upDirection = false;
            }
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - floatStep, transform.position.z);
            if (transform.position.y <= bottomBoundary)
            {
                upDirection = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<ThirdPersonPlayerController>();

        if (player)
        {
            player.TakeDamage(collisionDamage, ThirdPersonPlayerController.DAMAGE_TYPE.COLLISION);
        }
    }
}
