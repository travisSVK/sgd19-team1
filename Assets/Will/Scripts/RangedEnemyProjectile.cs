﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyProjectile : MonoBehaviour
{
    public float timeToLive = 2f;
    public int damage = 10;
    public Vector3 myScale;
    public float growthMultiplier = 0.25f;


    private bool isGrowing = false;
    private float lifetime;

    private void OnEnable()
    {
        GetComponent<Renderer>().enabled = true;
        lifetime = 0.0f;
    }

    void Start()
    {
        lifetime = 0.0f;
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<ThirdPersonPlayerController>();
        
        if (player != null)
        {
            player.TakeDamage(damage, ThirdPersonPlayerController.DAMAGE_TYPE.RANGED);
            StartCoroutine(Burst());
        }
    }

    private IEnumerator Burst()
    {
        GetComponent<Renderer>().enabled = false;
        transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(transform.GetChild(0).GetComponent<ParticleSystem>().main.duration);
        gameObject.SetActive(false);
    }

    public Vector3 GetDefaultScale()
    {
        return myScale;
    }

    public void StartGrowing()
    {
        isGrowing = true;
    }

    public void StopGrowing()
    {
        isGrowing = false;
    }

    void Update()
    {
        if (isGrowing && (myScale.x > transform.localScale.x))
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * growthMultiplier, transform.localScale.y + Time.deltaTime * growthMultiplier, transform.localScale.z + Time.deltaTime * growthMultiplier);
        }
        else
        {
            lifetime += Time.deltaTime;
        }

        if (lifetime > timeToLive)
        {
            gameObject.SetActive(false);
            lifetime = 0.0f;
        }
    }
}
