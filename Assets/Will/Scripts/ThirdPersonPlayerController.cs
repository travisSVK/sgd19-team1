﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonPlayerController : MonoBehaviour
{
    GameController gc;

    CharacterController characterController;

    public int hp = 100;

    public float forwardVelocity = 6.0f;
    public float sideVelocity = 0.6f;
    public float shootingBackoffRate = 0.5f;
    public float shootingSpeed = 20.0f;
    public GameObject projectile;
    public float projectileSpawnDistance = 2.0f;
    public float projectileSpawnUpperOffset = 2.0f;
    public float crosshairSensitivity = 0.1f;
    public float maxCrosshairSidesOffset = 2.0f;
    public float maxCrosshairUpperOffset = 2.0f;
    public float maxCrosshairLowerOffset = 1.7f;

    private GameObject crossHair;
    private float lastShot = 0.0f;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 shootDirection = Vector3.zero;
    private ObjectPool projectilePool;
    private bool isShooting = true;
    private bool isDead = false;
    private bool isColliding = false;
    private Animator animator;

    public enum DAMAGE_TYPE
    {
        RANGED,
        MELEE,
        STATIC,
        COLLISION
    }

    void Start()
    {
        lastShot = Time.time;
        characterController = GetComponent<CharacterController>();
        shootDirection = transform.forward;
        crossHair = transform.GetChild(0).gameObject;
        projectilePool = GetComponent<ObjectPool>();
        //crossHair = transform.GetChild(0).gameObject;
        gc = GameObject.Find("GameController").GetComponent<GameController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (isDead)
        {
            moveDirection = new Vector3(moveDirection.x, moveDirection.y - 0.01f, 1.0f);
            characterController.Move(moveDirection * Time.deltaTime);
            return;
        }
        moveDirection = new Vector3(Input.GetAxis("Horizontal") * sideVelocity, Input.GetAxis("Vertical") * sideVelocity, 1.0f * forwardVelocity);
        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);



        float rightVertical = Input.GetAxis("RVertical");
        float rightHorizontal = Input.GetAxis("RHorizontal");
        // If right joystick moved, move crosshair
        if (rightVertical != 0.0f || rightHorizontal != 0.0f)
        {
            var newX = crossHair.transform.position.x + rightHorizontal * crosshairSensitivity;
            var newY = crossHair.transform.position.y + rightVertical * crosshairSensitivity;

            crossHair.transform.position = new Vector3(newX, newY, crossHair.transform.position.z);
            //shootDirection += new Vector3(rightHorizontal * crosshairSensitivity, rightVertical * crosshairSensitivity, 0.0f);

            if (crossHair.transform.position.x < -maxCrosshairSidesOffset)
            {
                crossHair.transform.position = new Vector3(-maxCrosshairSidesOffset, crossHair.transform.position.y, crossHair.transform.position.z);
            }
            if (crossHair.transform.position.x > maxCrosshairSidesOffset)
            {
                crossHair.transform.position = new Vector3(maxCrosshairSidesOffset, crossHair.transform.position.y, crossHair.transform.position.z);
            }
            if (crossHair.transform.position.y < maxCrosshairLowerOffset)
            {
                crossHair.transform.position = new Vector3(crossHair.transform.position.x, maxCrosshairLowerOffset, crossHair.transform.position.z);
            }
            if (crossHair.transform.position.y > maxCrosshairUpperOffset)
            {
                crossHair.transform.position = new Vector3(crossHair.transform.position.x, maxCrosshairUpperOffset, crossHair.transform.position.z);
            }

            //Vector3 heading = crossHair.transform.position - transform.position;
            //float distance = heading.magnitude;
            //Vector3 direction = heading / distance;
            //shootDirection = direction;
        }

        // Instantiate projectiles
        if (isShooting && (Time.time - lastShot >= shootingBackoffRate))
        {
            lastShot = Time.time;
            Shoot();
        }
    }

    public void MoveForward()
    {
        //moveDirection = new Vector3(0.0f, 0.0f, 1.0f) * velocity;
        //characterController.Move(moveDirection * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDirection.z * Time.deltaTime);
    }

    public bool IsPlayerShooting()
    {
        return isShooting;
    }

    public void SetIsDead()
    {
        isDead = true;
        Transform exhausts = transform.Find("Exhausts");
        if (exhausts)
        {
            exhausts.gameObject.SetActive(false);
        }
    }

    public void EnableShooting()
    {
        isShooting = true;
        if (!crossHair)
        {
            crossHair = transform.Find("Crosshair").gameObject;
        }
        if (!crossHair.activeInHierarchy)
        {
            crossHair.SetActive(true);
        }
    }

    public void DisableShooting()
    {
        isShooting = false;
        if (!crossHair)
        {
            crossHair = transform.Find("Crosshair").gameObject;
        }
        if (crossHair.activeInHierarchy)
        {
            crossHair.SetActive(false);
        }
    }

    private void Shoot()
    {
        if (projectile)
        {
            GameObject newProjectile = projectilePool.GetPooledObject();
            if (newProjectile)
            {
                Rigidbody rb = newProjectile.GetComponent<Rigidbody>();

                //newProjectile.transform.Translate(transform.position + transform.forward);
                newProjectile.transform.position = new Vector3(transform.position.x, transform.position.y + projectileSpawnUpperOffset, transform.position.z + projectileSpawnDistance);
                //newProjectile.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + projectileSpawnDistance);
                //newProjectile.transform.position += new Vector3(0, 0, projectileSpawnDistance);
                //newProjectile.transform.Translate(transform.position + transform.right);
                newProjectile.GetComponent<ThirdPersonProjectile>().SetOldPosition(newProjectile.transform.position);

                Vector3 heading = crossHair.transform.position - newProjectile.transform.position;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;
                shootDirection = direction;

                rb.velocity = shootDirection * shootingSpeed;

                //Debug.DrawRay(transform.position, shootDirection * 100, Color.green);
                Debug.DrawRay(newProjectile.transform.position, rb.velocity, Color.red);
                //rb.velocity = (crossHair.transform.position - transform.position) * shootingSpeed;
                //Debug.Log(rb.velocity);
            }
        }
    }

    public void TakeDamage(int dmg, DAMAGE_TYPE damageType)
    {
        if (!isDead && !isColliding)
        {
            hp -= dmg;

            if (hp <= 0)
            {
                gc.KillPlayer(gameObject);
                isDead = true;
                animator.SetTrigger("Death");
            }
            else
            {
                if (damageType == DAMAGE_TYPE.COLLISION)
                {
                    animator.SetTrigger("Collision");
                    isColliding = true;
                }
                gc.StartCoroutine(gc.DamageFlash());
            }
        }
    }

    public void Heal(int heal)
    {
        hp += heal;
        if (hp > 100)
        {
            hp = 100;
        } 
    }

    // event called on collision animation end
    public void CollisionEnd()
    {
        isColliding = false;
    }

    // event called on death animation end
    public void Death()
    {
        gc.PlayerDead(gameObject);
    }

    public int GetHP()
    {
        return hp;
    }
}
