﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemyController : MonoBehaviour
{
    public int hp = 100;
    public float moveForwardSpeed = 3f;
    public float moveSidewaysSpeed = 0.5f;
    public float playerApproachDistance = 1.5f;
    public float playerAttackDistance = 1.0f;

    private GameObject player;
    private Animator animator;
    private bool playerApproaching = false;
    private bool attack = false;
    private Material material;
    private Color materialColor;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
        Renderer renderer = transform.GetChild(0).GetComponent<Renderer>();
        material = renderer.material;
        materialColor = material.GetColor("_EmissiveColor");
    }

    private void Update()
    {
        if (player != null)
        {
            // Move if not too close to player
            if (!playerApproaching && ((transform.position.z - player.transform.position.z) <= playerApproachDistance))
            {
                playerApproaching = true;
                animator.SetTrigger("PlayerNear");
            }
            if (!attack && ((transform.position.z - player.transform.position.z) <= playerAttackDistance))
            {
                attack = true;
                animator.SetTrigger("Attack");
            }
        }
    }

    public void AttackFinished()
    {
        gameObject.SetActive(false);
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            animator.SetTrigger("Death");
        }
        else
        {
            StartCoroutine(Flash());
        }
    }

    public void OnDeath()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator Flash()
    {
        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = new Color(0.245283f, 0f, 0f);
        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        material.SetColor("_EmissiveColor", finalColor);

        yield return new WaitForSeconds(0.2f);

        material.SetColor("_EmissiveColor", materialColor);
    }
}
