﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMovementController : MonoBehaviour
{
    public float velocity = 6.0f;
    public bool isShootingLevelPart = false;

    private Vector3 moveDirection = Vector3.zero;
    // Start is called before the first frame update

    private void OnEnable()
    {
    }

    void Start()
    {
        if (gameObject.tag.Equals("firstLevel"))
        {
            SetIsShootingLevelPart();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //moveDirection = new Vector3(0.0f, 0.0f, -1.0f);
        //transform.Translate(moveDirection * velocity * Time.deltaTime);
    }

    public void SetIsShootingLevelPart()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        ThirdPersonPlayerController playerController = player.GetComponent<ThirdPersonPlayerController>();
        if (playerController)
        {
            if (isShootingLevelPart)
            {
                playerController.EnableShooting();
            }
            else
            {
                playerController.DisableShooting();
            }
        }
    }
}
