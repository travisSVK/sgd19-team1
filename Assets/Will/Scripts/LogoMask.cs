﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoMask : MonoBehaviour
{
    public float startX = -1013f;
    public float finalX = 1013f;
    public float speed = 0.15f;
    private Vector3 maskedImagePos;
    private Vector3 shift;
    public bool moveLocally = false;

    private void Awake()
    {
        if (!moveLocally)
        {
            maskedImagePos = transform.GetChild(0).transform.position;
        }
        else
        {
            maskedImagePos = transform.GetChild(0).transform.position;
            shift = maskedImagePos - transform.parent.parent.parent.position;
        }
    }

    void Update()
    {
        if (transform.localPosition.x < finalX)
        {
            transform.localPosition += new Vector3(speed * Time.deltaTime, 0, 0);
        }
        else
        {
            transform.localPosition = new Vector3(startX, transform.localPosition.y, transform.localPosition.z);
        }

        if (!moveLocally)
        {
            transform.GetChild(0).transform.position = maskedImagePos;
        }
        else
        {
            transform.GetChild(0).transform.position = transform.parent.parent.parent.position + shift;
        }
    }
}
