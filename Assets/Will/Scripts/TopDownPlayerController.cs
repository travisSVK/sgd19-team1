﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownPlayerController : MonoBehaviour
{
    public float movementSpeed = 5.0f;
    public float rotationSpeed = 100.0f;
    public float shootingSpeed = 5.0f;
    public float shootingDelta = 0.2f;
    private float lastShot;
    public GameObject projectilePrefab;
    private Vector3 originalForward;

    private void Start()
    {
        originalForward = transform.forward;
        lastShot = Time.time;
    }

    private void FixedUpdate()
    {
        Move();
        Shoot();
    }

    private void Move()
    {
        // Left joystick input
        float translation = Input.GetAxis("Vertical") * movementSpeed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Move speed by time not by frames
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
    }

    private void Shoot()
    {
        // Pause between shots
        if (Time.time - lastShot >= shootingDelta)
        {
            lastShot = Time.time;

            // Right joystick input
            float rightVertical = Input.GetAxis("RVertical");
            float rightHorizontal = Input.GetAxis("RHorizontal");

            // If right joystick moved (trying to shoot)
            if (rightVertical != 0.0f || rightHorizontal != 0.0f)
            {
                GameObject projectile = Instantiate(projectilePrefab) as GameObject;
                Rigidbody rb = projectile.GetComponent<Rigidbody>();
                Vector3 rightJoystick = new Vector3(rightHorizontal, 0, rightVertical);

                // Rotation "magic" so player can shoot whichever direction
                var angle = Vector3.Angle(transform.forward, originalForward);
                // Unity return angles only between 0-180, 
                // when you should receive 180+ angles are decreasing back to 0
                if (transform.forward.x < 0.0f)
                {
                    angle = 180.0f + (180.0f - angle);
                }
                Vector3 forwardAngle = Quaternion.Euler(0, angle, 0) * rightJoystick;

                projectile.transform.Translate(transform.position + forwardAngle);
                rb.AddForce(forwardAngle * shootingSpeed, ForceMode.Impulse);
            }
        }
    }
}
