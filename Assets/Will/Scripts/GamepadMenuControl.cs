﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GamepadMenuControl : MonoBehaviour
{
    public EventSystem eventSystem;
    public GameObject selectedObj;
    private bool buttonSelected = false;

    void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && !buttonSelected)
        {
            buttonSelected = true;
            eventSystem.SetSelectedGameObject(selectedObj);
        }
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }
}
