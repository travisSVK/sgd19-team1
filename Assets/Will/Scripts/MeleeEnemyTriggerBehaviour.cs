﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemyTriggerBehaviour : MonoBehaviour
{
    public int damage = 20;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        ThirdPersonPlayerController player = other.gameObject.GetComponent<ThirdPersonPlayerController>();
        if (player)
        {
            player.TakeDamage(damage, ThirdPersonPlayerController.DAMAGE_TYPE.MELEE);
        }
    }
}
