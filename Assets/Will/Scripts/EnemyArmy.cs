﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmy : MonoBehaviour
{
    public enum EnemyTypes
    {
        Spider = 0,
        Reaper = 1,
        Pumpkin = 2
    }

    private delegate GameObject Create(Vector3 position, Quaternion rotation);
    private Create[] constructors;
    private ObjectPool[] objectPools;

    // Start is called before the first frame update
    void Start()
    {
        objectPools = GetComponents<ObjectPool>();
        //constructors = new Create[]
        //{
        //    (position, rotation) => Instantiate(spider, position, rotation),
        //    (position, rotation) => Instantiate(reaper, position, rotation),
        //    (position, rotation) => Instantiate(pumpkin, position, rotation)
        //};
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int SizeOfRanged()
    {
        int i = 0;
        foreach (Transform child in transform)
        {
            RangedEnemyController enemyController = child.gameObject.GetComponentInChildren<RangedEnemyController>();
            if (enemyController && child.gameObject.activeInHierarchy)
            {
                i++;
            }
        }
        return i;
    }

    public int Size()
    {
        int i = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeInHierarchy)
            {
                i++;
            }
        }
        return i;
    }

    public void SpawnEnemy(EnemyTypes enemyType, Vector3 position, Quaternion rotation)
    {
        GameObject enemy = objectPools[(int)enemyType].GetPooledObject();
        if (enemy)
        {
            enemy.transform.position = position;
            enemy.transform.rotation = rotation;
        }
        //constructors[(int)enemyType](position, rotation);
    }
}
