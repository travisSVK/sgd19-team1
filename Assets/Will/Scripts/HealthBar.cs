﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private ThirdPersonPlayerController player;
    public AudioSource heartbeat;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ThirdPersonPlayerController>();
    }

    void Update()
    {
        int color = player.GetHP() / 25;

        switch (color)
        {
            case 0:
                if (GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().color = Color.red;
                }
                else if (GetComponent<Image>())
                {
                    GetComponent<Image>().color = Color.red;
                }
                if (!heartbeat.isPlaying)
                {
                    heartbeat.Play();
                }
                break;
            case 1:
                if (GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().color = new Color(1f, 0.6f, 0f, 1f);
                }
                else if (GetComponent<Image>())
                {
                    GetComponent<Image>().color = new Color(1f, 0.6f, 0f, 1f);
                }
                if (heartbeat.isPlaying)
                {
                    heartbeat.Stop();
                }
                break;
            case 2:
                if (GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().color = Color.yellow;
                }
                else if (GetComponent<Image>())
                {
                    GetComponent<Image>().color = Color.yellow;
                }
                if (heartbeat.isPlaying)
                {
                    heartbeat.Stop();
                }
                break;
            case 3:
                if (GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().color = Color.green;
                }
                else if (GetComponent<Image>())
                {
                    GetComponent<Image>().color = Color.green;
                }
                if (heartbeat.isPlaying)
                {
                    heartbeat.Stop();
                }
                break;
            case 4:
                if (GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().color = Color.green;
                }
                else if (GetComponent<Image>())
                {
                    GetComponent<Image>().color = Color.green;
                }
                if (heartbeat.isPlaying)
                {
                    heartbeat.Stop();
                }
                break;
        }
    }
}
