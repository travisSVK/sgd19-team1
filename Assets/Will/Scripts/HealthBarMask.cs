﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarMask : MonoBehaviour
{
    public float startY = -0.0137f;
    public float finalY = 0.01f;
    public float speed = 0.015f;

    void Update()
    {
        if (transform.localPosition.y < finalY)
        {
            transform.localPosition += new Vector3(0, speed * Time.deltaTime, 0);
        }
        else
        {
            transform.localPosition = new Vector3(transform.localPosition.x, startY, transform.localPosition.z);
        }
    }
}
