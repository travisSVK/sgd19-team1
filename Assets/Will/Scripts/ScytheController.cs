﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScytheController : MonoBehaviour
{
    public int damage = 40;
    public float attackSpeed = 1.0f;
    public Vector3 leftTopPosition;
    public Vector3 leftBottomPosition;
    public Vector3 rightTopPosition;
    public Vector3 rightBottomPosition;

    private bool leftToRight = false;
    private bool rightToLeft = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (leftToRight)
        {
            float step = attackSpeed * Time.deltaTime; // calculate distance to move
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, rightBottomPosition, step);
            if (Vector3.Distance(transform.localPosition, rightBottomPosition) < 0.001f)
            {
                leftToRight = false;
            }
        }
        else if (rightToLeft)
        {
            float step = attackSpeed * Time.deltaTime; // calculate distance to move
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, leftBottomPosition, step);
            if (Vector3.Distance(transform.localPosition, leftBottomPosition) < 0.001f)
            {
                rightToLeft = false;
            }
        }
    }

    public void StartLeftToRightAttack()
    {
        transform.localPosition = leftTopPosition;
        leftToRight = true;
    }

    public void StartRightToLeftAttack()
    {
        transform.localPosition = rightTopPosition;
        rightToLeft = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        ThirdPersonPlayerController player = other.GetComponent<ThirdPersonPlayerController>();
        if (player)
        {
            player.TakeDamage(damage, ThirdPersonPlayerController.DAMAGE_TYPE.MELEE);
        }
    }
}
