﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossController : MonoBehaviour
{
    public int hp = 500;
    public float fightDistance = 2.0f;
    public float fightUpDistance = 0.5f;
    public float vulnerableTimeDuration = 4.0f;
    public float pumpkingAttackDuration = 4.0f;
    public float pumpkingSpawnBackoffRate = 0.2f;
    public float idleTimeAfterAction = 2.0f;
    public float playerDownBoundariesAddition = -0.2f;
    public Vector3 leftPumpkingSpawnPoint = new Vector3(-0.638f, 0.059f, 18.257f);
    public Vector3 rightPumpkingSpawnPoint = new Vector3(0.567f, 0.067f, 18.138f);

    private bool isDead = false;
    private bool bossArrived = false;
    private bool inAction = false;
    private bool isVulnerable = false;
    private bool isPumpkinAttack = false;
    private bool isLeftPumpkingSpawn = false;
    private int damageTaken = 0;
    private float traveledDistance = 0.0f;
    private float currentTimeVulnerable = 0.0f;
    private float currentTimePumpkingAttack = 0.0f;
    private float currentPumpkingSpawnTime = 0.0f;
    private float idle = 0.0f;
    private delegate void Movement();
    private Movement[] movements;
    private Animator animator;
    private ScytheController scytheController;
    private EnemyArmy enemyArmy;
    private Color materialColor;
    private Material material;

    public GameObject quitButton;
    public GameObject cutscene;
    public GameObject cutsceneBackground;
    public float cutsceneDuration = 4f;
    public float fadeInRate = 1f;
    public float fadeOutRate = 2f;
    private bool fadedIn = false;
    private bool fadedOut = false;
    // Start is called before the first frame update
    void Start()
    {
        movements = new Movement[]
        {
            () => Scream(),
            () => SpawnEnemies()
        };
        animator = GetComponent<Animator>();
        scytheController = transform.parent.GetComponentInChildren<ScytheController>();
        enemyArmy = transform.parent.GetComponent<EnemyArmy>();
        Renderer renderer = transform.GetChild(0).GetComponent<Renderer>();
        material = renderer.material;
        materialColor = material.GetColor("_EmissiveColor");
        transform.position = new Vector3(transform.position.x, transform.position.y - 5.0f, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (bossArrived)
        {
            // move back
            if (traveledDistance < fightDistance)
            {
                traveledDistance += Time.deltaTime;
                transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * 0.25f, transform.position.z + Time.deltaTime);
                if (traveledDistance >= fightDistance)
                {
                    GameObject player = GameObject.FindGameObjectWithTag("Player");
                    ThirdPersonPlayerController playerController = player.GetComponent<ThirdPersonPlayerController>();
                    ScreenBoundaries screenBoundaries = player.GetComponent<ScreenBoundaries>();
                    screenBoundaries.SetBottomBoundary(screenBoundaries.GetBottomBoundary() + playerDownBoundariesAddition);
                    playerController.EnableShooting();
                }
                return;
            }
            // not currently in action
            if (!inAction)
            {
                // check for the idle state between actions
                idle += Time.deltaTime;
                if (idle >= idleTimeAfterAction)
                {
                    idle = 0.0f;
                    int currentAction = Random.Range(0, movements.Length);
                    inAction = true;
                    movements[currentAction]();
                }
            }
            else
            {
                if (isVulnerable)
                {
                    currentTimeVulnerable += Time.deltaTime;
                    if (currentTimeVulnerable >= vulnerableTimeDuration)
                    {
                        currentTimeVulnerable = 0.0f;
                        isVulnerable = false;
                        if (damageTaken > 0)
                        {
                            animator.SetTrigger("BossDamaged");
                            damageTaken = 0;
                        }
                        else
                        {
                            animator.SetTrigger("BossNotDamaged");
                            inAction = false;
                        }
                    }
                }
                else if (isPumpkinAttack)
                {
                    currentTimePumpkingAttack += Time.deltaTime;
                    if (currentTimePumpkingAttack >= pumpkingAttackDuration)
                    {
                        currentTimePumpkingAttack = 0.0f;
                        isPumpkinAttack = false;
                        inAction = false;
                        animator.SetTrigger("EndEnemySpawn");
                    }
                    else
                    {
                        currentPumpkingSpawnTime += Time.deltaTime;
                        if (currentPumpkingSpawnTime >= pumpkingSpawnBackoffRate)
                        {
                            // spawn pumpking
                            currentPumpkingSpawnTime = 0.0f;
                            Vector3 spawnPosition;
                            if (isLeftPumpkingSpawn)
                            {
                                spawnPosition = leftPumpkingSpawnPoint;
                                isLeftPumpkingSpawn = false;
                            }
                            else
                            {
                                spawnPosition = rightPumpkingSpawnPoint;
                                isLeftPumpkingSpawn = true;
                            }
                            enemyArmy.SpawnEnemy(EnemyArmy.EnemyTypes.Pumpkin, spawnPosition, transform.rotation);
                        }
                    }
                }
            }
        }
    }

    //event called after the boss has arrived
    public void BossArrival()
    {
        bossArrived = true;
    }

    //event called after the scythe attack finished
    public void ScytheAttackFinished()
    {
        inAction = false;
    }

    //event called after the left scythe attack finished
    public void LeftToRightAttack()
    {
        scytheController.StartLeftToRightAttack();
    }

    //event called after the right scythe attack finished
    public void RightToLeftAttack()
    {
        scytheController.StartRightToLeftAttack();
    }

    //event called after the boss died
    public void OnDeath()
    {
        // TODO: endscreen??
        //Debug.Log("Game overino");
        StartCoroutine(GameWon());
    }

    IEnumerator GameWon()
    {
        // fade in cutscene
        cutscene.SetActive(true);
        cutscene.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        //cutscene.GetComponent<Image>().CrossFadeAlpha(1, 0.7f, false);
        //sceneSettings.SetActive(true);

        while (!fadedIn && !fadedOut)
        {
            if (fadedIn)
            {
                if (!cutsceneBackground.activeInHierarchy)
                {
                    cutsceneBackground.SetActive(true);
                }

                cutsceneDuration -= Time.deltaTime;

                if (cutsceneDuration <= 0.01f)
                {
                    //fadedOut = FadeCutscene(1f, 0.008f, fadeOutRate);
                    fadedOut = true;

                    if (fadedOut)
                    {
                        quitButton.SetActive(true);
                    }
                }
            }
            else
            {
                //fadeInRate += Time.deltaTime * 1;
                fadedIn = FadeCutscene(1f, 0.2f, fadeInRate);

                if (fadedIn)
                {
                    cutsceneDuration -= Time.deltaTime;
                }
            }

            yield return null;
        }
    }

    private bool FadeCutscene(float alpha, float diff, float fadeRate)
    {
        Color curColor = cutscene.GetComponent<Image>().color;
        float alphaDiff = Mathf.Abs(curColor.a - alpha);
        if (alphaDiff > diff)
        {
            curColor.a = Mathf.Lerp(curColor.a, alpha, fadeRate * Time.deltaTime);
            cutscene.GetComponent<Image>().color = curColor;
            return false;
        }
        else
        {
            return true;
        }
    }

    public void StartBoss()
    {
        animator.SetTrigger("BossArrival");
    }

    public void BossArriving()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 5.0f, transform.position.z);
    }

    private void Scream()
    {
        animator.SetTrigger("Scream");
        isVulnerable = true;
    }

    private void SpawnEnemies()
    {
        animator.SetTrigger("SpawnEnemies");
        isPumpkinAttack = true;
    }

    public void TakeDamage(int dmg)
    {
        if (isVulnerable)
        {
            hp -= dmg;
            damageTaken += dmg;
            if (hp <= 0)
            {
                isDead = true;
                animator.SetTrigger("Death");
            }
            else
            {
                StartCoroutine(Flash());
            }
        }
    }

    private IEnumerator Flash()
    {
        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = new Color(0.245283f, 0f, 0f);
        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        material.SetColor("_EmissiveColor", finalColor);

        yield return new WaitForSeconds(0.2f);

        material.SetColor("_EmissiveColor", materialColor);
    }
}
