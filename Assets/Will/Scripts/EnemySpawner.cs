﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int playerHealth;       
    public GameObject enemy;                
    public float spawnTime = 3f;           
    public Transform[] spawnPoints;        

    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn()
    {
        // If the player has no health left...
        if (playerHealth <= 0)
        {
            return;
        }

        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Vector3 pos = spawnPoints[spawnPointIndex].position;
        pos.y += enemy.transform.position.y;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(enemy, pos, spawnPoints[spawnPointIndex].rotation);
    }
}
