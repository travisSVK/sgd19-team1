﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBoundaries : MonoBehaviour
{
    public float leftBoundManualSet = 0.0f;
    public float rightBoundManualSet = 0.0f;
    public float bottomBoundManualSet = 0.0f;
    public float topBoundManualSet = 0.0f;

    private float leftBound;
    private float rightBound;
    private float bottomBound;
    private float topBound;
    // Start is called before the first frame update
    void Start()
    {
        //screenBoundaries = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        if (rightBoundManualSet == 0.0f)
        {
            rightBound = Camera.main.ViewportToWorldPoint(Vector3.one).x;
            leftBound = -1 * rightBound;
        }
        else
        {
            rightBound = rightBoundManualSet;
            leftBound = leftBoundManualSet;
        }

        if (topBoundManualSet == 0.0f)
        {
            topBound = Camera.main.ViewportToWorldPoint(Vector3.one).y;
            bottomBound = -1 * topBound;
        }
        else
        {
            topBound = topBoundManualSet;
            bottomBound = bottomBoundManualSet;
        }
    }

    public void SetBottomBoundary(float boundary)
    {
        bottomBound = boundary;
    }

    public float GetBottomBoundary()
    {
        return bottomBound;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        if (viewPos.x <= leftBound)
        {
            viewPos.x = leftBound;
        }
        else if (viewPos.x >= rightBound)
        {
            viewPos.x = rightBound;
        }
        if (viewPos.y < bottomBound)
        {
            viewPos.y = bottomBound;
        }
        else if (viewPos.y >= topBound)
        {
            viewPos.y = topBound;
        }
        transform.position = viewPos;
    }
}
