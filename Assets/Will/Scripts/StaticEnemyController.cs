﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticEnemyController : MonoBehaviour
{
    public int hp = 100;
    private int maxHp;
    public float playerAttackDistance = 0.8f;

    private GameObject player;
    private Animator animator;
    private bool attackStarted = false;
    private Material material;
    private Color materialColor;

    private void OnEnable()
    {
        Transform renderedObj = transform.GetChild(0);
        renderedObj.GetComponent<Renderer>().enabled = true;
        hp = maxHp;
    }

    // Start is called before the first frame update
    void Start()
    {
        maxHp = hp;
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
        Renderer renderer = transform.GetChild(0).GetComponent<Renderer>();
        material = renderer.material;
        materialColor = material.GetColor("_EmissiveColor");
    }

    private void Update()
    {
        if (player != null)
        {
            // Attack if close to player
            if (((transform.position.z - player.transform.position.z) <= playerAttackDistance) && !attackStarted)
            {
                attackStarted = true;
                Attack();
            }

            if (attackStarted && animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void Attack()
    {
        if (animator)
        {
            animator.SetTrigger("Attack");
        }
    }

    public void AttackFinished()
    {
        gameObject.SetActive(false);
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            StartCoroutine(Die());
        }
        else
        {
            StartCoroutine(Flash());
        }
    }

    private IEnumerator Die()
    {
        animator.SetBool("Die", true);
        yield return new WaitForSecondsRealtime(animator.GetCurrentAnimatorStateInfo(0).length + 0.02f);
        animator.SetBool("Die", false);

        Transform renderedObj = transform.GetChild(0);
        renderedObj.GetComponent<Renderer>().enabled = false;
        transform.Find("DeathAnim").GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(transform.Find("DeathAnim").GetComponent<ParticleSystem>().main.duration);
        gameObject.SetActive(false);
    }

    private IEnumerator Flash()
    {
        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = new Color(0.245283f, 0f, 0f);
        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        material.SetColor("_EmissiveColor", finalColor);

        yield return new WaitForSeconds(0.2f);

        material.SetColor("_EmissiveColor", materialColor);
    }
}
