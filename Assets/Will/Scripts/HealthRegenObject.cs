﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRegenObject : MonoBehaviour
{
    public int healAmount = 10;
    public float rotationSpeed = 60f;

    void Update()
    {
        transform.Rotate(new Vector3(1f, 1f, 1f), Time.deltaTime * rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<ThirdPersonPlayerController>();

        if (player != null)
        {
            player.Heal(healAmount);
            GameObject.FindGameObjectWithTag("HealSound").GetComponent<AudioSource>().Play();
            Destroy(gameObject);
        }
    }
}
