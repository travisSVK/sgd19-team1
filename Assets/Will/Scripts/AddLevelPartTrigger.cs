﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLevelPartTrigger : MonoBehaviour
{
    public Transform nextLevelPart;
    public Transform defaultLevelPart;
    public float offset = 15.0f;

    private bool activated = false;

    private void OnEnable()
    {
        activated = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //enemyArmyComponent = GetComponentInChildren<EnemyArmy>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        ThirdPersonPlayerController player = other.GetComponent<ThirdPersonPlayerController>();
        if (player && !activated)
        {
            activated = true;
            Transform enemyArmy = transform.parent.GetChild(0);
            EnemyArmy enemyArmyComponent = enemyArmy.GetComponent<EnemyArmy>();
            if (enemyArmyComponent.SizeOfRanged() > 0)
            {
                // if the default level is turning on default level
                defaultLevelPart.position = new Vector3(transform.parent.position.x, transform.parent.position.y, transform.parent.position.z + offset);
                defaultLevelPart.gameObject.SetActive(true);
                //Instantiate(defaultLevelPart, new Vector3(defaultLevelPart.position.x, defaultLevelPart.position.y, transform.parent.position.z + offset), defaultLevelPart.rotation);
                AddLevelPartTrigger levelTrigger = defaultLevelPart.GetComponentInChildren<AddLevelPartTrigger>();
                levelTrigger.nextLevelPart = nextLevelPart;

                //Transform army = transform.GetChild(0);
                foreach (Transform child in enemyArmy.transform)
                {
                    RangedEnemyController enemyController = child.gameObject.GetComponentInChildren<RangedEnemyController>();
                    if (enemyController && child.gameObject.activeInHierarchy)
                    {
                        Vector3 tmpPosition = child.position;
                        child.parent = defaultLevelPart.Find("EnemyArmy");
                        child.position = tmpPosition;
                    }
                }
            }
            else
            {
                nextLevelPart.position = new Vector3(transform.parent.position.x, transform.parent.position.y, transform.parent.position.z + offset);
                nextLevelPart.gameObject.SetActive(true);
            }
        }
    }
}
