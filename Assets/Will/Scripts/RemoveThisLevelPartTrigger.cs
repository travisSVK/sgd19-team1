﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveThisLevelPartTrigger : MonoBehaviour
{
    public bool isFinal = false;
    public GameObject finalFight;

    private bool activated = false;

    private void OnEnable()
    {
        activated = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerExit(Collider other)
    {
        ThirdPersonPlayerController player = other.GetComponent<ThirdPersonPlayerController>();
        if (player && !activated)
        {
            activated = true;
            GameObject gameControllerGo = GameObject.FindGameObjectWithTag("GameController");
            GameController gameController = gameControllerGo.GetComponent<GameController>();
            Transform spawnPoint = null;
            if (isFinal && finalFight)
            {
                player.forwardVelocity = 0.0f;
                player.DisableShooting();
                //finalFight.gameObject.SetActive(true);
                spawnPoint = finalFight.transform.Find("SpawnPoint");
                BossController bossController = GameObject.FindGameObjectWithTag("boss").GetComponent<BossController>();
                if (bossController)
                {
                    bossController.StartBoss();
                }
            }
            else
            {
                AddLevelPartTrigger levelPartTrigger = transform.parent.gameObject.transform.GetComponentInChildren<AddLevelPartTrigger>();

                if (levelPartTrigger.nextLevelPart.gameObject.activeInHierarchy)
                {
                    spawnPoint = levelPartTrigger.nextLevelPart.Find("SpawnPoint");
                    LevelMovementController levelMovementController = levelPartTrigger.nextLevelPart.gameObject.GetComponent<LevelMovementController>();
                    if (levelMovementController)
                    {
                        levelMovementController.SetIsShootingLevelPart();
                    }
                }
                else if (levelPartTrigger.defaultLevelPart.gameObject.activeInHierarchy)
                {
                    spawnPoint = levelPartTrigger.defaultLevelPart.Find("SpawnPoint");
                    LevelMovementController levelMovementController = levelPartTrigger.defaultLevelPart.gameObject.GetComponent<LevelMovementController>();
                    if (levelMovementController)
                    {
                        levelMovementController.SetIsShootingLevelPart();
                    }
                }
            }
            if (spawnPoint)
            {
                gameController.SetSpawnPoint(spawnPoint.gameObject);
            }
            transform.parent.gameObject.SetActive(false);
        }
    }
}
