﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulController : MonoBehaviour
{
    private BossController bossController;
    // Start is called before the first frame update
    void Start()
    {
        bossController = GetComponentInParent<BossController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        ThirdPersonProjectile playerProjectile = other.GetComponent<ThirdPersonProjectile>();
        if (playerProjectile)
        {
            bossController.TakeDamage(playerProjectile.damage);
        }
    }
}
