﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyController : MonoBehaviour
{
    public int hp = 100;
    private int maxHp;
    public int damage = 10;
    public float moveForwardSpeed = 3f;
    public float moveSidewaysSpeed = 0.5f;
    public float maxDist = 40f;
    public float minDist = 0.8f;
    public GameObject[] waypoints;
    public float waypointDistance = 0.05f;
    public GameObject projectile;
    public float shootingSpeed = 0.1f;
    public float shootingBackoffRate = 0.2f;

    private GameObject player;
    private GameObject currentWaypoint;
    private int waypointIndex = 0;
    private float lastShot;
    private Animator animator;
    private ObjectPool projectilePool;
    private GameObject currentActiveProjectile;
    private Material material;
    private Color materialColor;

    private void Start()
    {
        maxHp = hp;

        lastShot = Time.time;

        transform.LookAt(-Camera.main.transform.forward);
        animator = GetComponent<Animator>();
        projectilePool = GetComponent<ObjectPool>();

        if (waypoints.Length > 0) {
            currentWaypoint = waypoints[waypointIndex];
        }
        Renderer renderer = transform.GetChild(0).GetComponent<Renderer>();
        material = renderer.material;
        materialColor = material.GetColor("_EmissiveColor");
    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {

            //MoveToWaypoint();

            // keep distance from player
            if ((transform.position.z - player.transform.position.z) <= minDist)
            {
                //transform.position += new Vector3(0, 0, (transform.forward * moveForwardSpeed * Time.deltaTime).z);
                transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + minDist);
            }

            // Shoot if not too far away from player
            if ((transform.position.z - player.transform.position.z) <= maxDist)
            {
                // Pause between shots
                if (Time.time - lastShot >= shootingBackoffRate)
                {
                    lastShot = Time.time;
                    Shoot();
                }
            }
        }
    }

    private void MoveToWaypoint()
    {
        // Check if 
        Vector3 directionToWaypoint = new Vector3(0, 0, 0);
        if (currentWaypoint != null)
        {
            if (Vector3.Distance(transform.position, currentWaypoint.transform.position) <= waypointDistance)
            {
                waypointIndex = (waypointIndex + 1) % waypoints.Length;
                currentWaypoint = waypoints[waypointIndex];
            }
            Vector3 heading = currentWaypoint.transform.position - transform.position;
            float distance = heading.magnitude;
            directionToWaypoint = heading / distance;
        }

        // Moves enemy towards current waypoint
        transform.position += directionToWaypoint * moveSidewaysSpeed * Time.deltaTime;
    }

    private void Shoot()
    {
        animator.SetTrigger("Attack");
    }

    public void AttackStarted()
    {
        if (projectile)
        {
            currentActiveProjectile = projectilePool.GetPooledObject();
            if (currentActiveProjectile)
            {
                Rigidbody rb = currentActiveProjectile.GetComponent<Rigidbody>();
                RangedEnemyProjectile rangedEnemyProjectile = currentActiveProjectile.GetComponent<RangedEnemyProjectile>();
                currentActiveProjectile.transform.position = new Vector3(transform.position.x, transform.position.y + 0.03f, transform.position.z - 0.03f);
                //newProjectile.transform.position = new Vector3(transform.position.x + transform.forward.x, transform.position.y + transform.forward.y, transform.position.z + transform.forward.z);
                Vector3 defaultScale = rangedEnemyProjectile.GetDefaultScale();
                currentActiveProjectile.transform.localScale = new Vector3(defaultScale.x / 20.0f, defaultScale.y / 20.0f, defaultScale.z / 20.0f);
                rb.constraints = RigidbodyConstraints.FreezePosition;
                rangedEnemyProjectile.StartGrowing();
            }
        }
    }

    public void AttackFinished()
    {
        if (currentActiveProjectile)
        {
            currentActiveProjectile.GetComponent<RangedEnemyProjectile>().StopGrowing();
            Rigidbody rb = currentActiveProjectile.GetComponent<Rigidbody>();
            //newProjectile.transform.position = new Vector3(transform.position.x + transform.forward.x, transform.position.y + transform.forward.y, transform.position.z + transform.forward.z);
            //newProjectile.transform.Translate(transform.position + new Vector3(transform.forward.x, transform.forward.y + 1.0f, transform.forward.z));
            rb.constraints = RigidbodyConstraints.None;
            rb.velocity = new Vector3(0,0,-1) * shootingSpeed;
        }
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;

        if (hp <= 0)
        {
            StartCoroutine(Die());
        }
        else
        {
            StartCoroutine(Flash());
        }
    }

    private IEnumerator Die()
    {
        animator.SetBool("Die", true);
        yield return new WaitForSecondsRealtime(animator.GetCurrentAnimatorStateInfo(0).length + 0.5f);
        animator.SetBool("Die", false);
        //hp = maxHp;
        gameObject.SetActive(false);
    }

    private IEnumerator Flash()
    {
        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = new Color(0.245283f, 0f, 0f);
        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        material.SetColor("_EmissiveColor", finalColor);

        yield return new WaitForSeconds(0.2f);

        material.SetColor("_EmissiveColor", materialColor);
    }
}
