﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject sceneSettings;
    public GameObject cutscene;
    public GameObject cutsceneBackground;
    public float cutsceneDuration = 4f;
    public float fadeInRate = 1f;
    public float fadeOutRate = 2f;
    private bool fadedIn = false;
    private bool fadedOut = false;

    public void Play()
    {
        StartCoroutine(LoadAsyncScene(1));
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator LoadAsyncScene(int id)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(id);
        asyncLoad.allowSceneActivation = false;

        // fade in cutscene
        cutscene.SetActive(true);
        cutscene.GetComponent<Image>().color = new Color(1,1,1,0);
        //cutscene.GetComponent<Image>().CrossFadeAlpha(1, 0.7f, false);
        sceneSettings.SetActive(true);

        GameObject.Find("Play").SetActive(false);
        GameObject.Find("Quit").SetActive(false);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            if (asyncLoad.progress >= 0.9f && fadedIn)
            {
                if (!cutsceneBackground.activeInHierarchy)
                {
                    cutsceneBackground.SetActive(true);
                }

                cutsceneDuration -= Time.deltaTime;

                if (cutsceneDuration <= 0.01f)
                {
                    fadedOut = FadeCutscene(0f, 0.008f, fadeOutRate);

                    if (fadedOut)
                    {
                        asyncLoad.allowSceneActivation = true;
                    }
                }
            }
            else
            {
                //fadeInRate += Time.deltaTime * 1;
                fadedIn = FadeCutscene(1f, 0.2f, fadeInRate);

                if (fadedIn)
                {
                    cutsceneDuration -= Time.deltaTime;
                }
            }

            yield return null;
        }

        // fade out cutscene
        //cutscene.GetComponent<Image>().CrossFadeAlpha(0, 1f, false);
    }

    private bool FadeCutscene(float alpha, float diff, float fadeRate)
    {
        Color curColor = cutscene.GetComponent<Image>().color;
        float alphaDiff = Mathf.Abs(curColor.a - alpha);
        if (alphaDiff > diff)
        {
            curColor.a = Mathf.Lerp(curColor.a, alpha, fadeRate * Time.deltaTime);
            cutscene.GetComponent<Image>().color = curColor;
            return false;
        }
        else
        {
            return true;
        }
    }
}
