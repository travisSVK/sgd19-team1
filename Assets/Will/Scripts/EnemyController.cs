﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameObject player;
    public GameObject projectilePrefab;
    public int moveSpeed = 3;
    public int maxDist = 15;
    public int minDist = 5;
    public float shootingSpeed = 5.0f;
    public float shootingDelta = 0.2f;
    private float lastShot;

    private void Start()
    {
        lastShot = Time.time;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        // Rotates enemy towards player
        transform.LookAt(player.transform);

        // Move and shoot if not too close to player
        if (Vector3.Distance(transform.position, player.transform.position) >= minDist)
        {
            // Moves enemy towards player
            transform.position += transform.forward * moveSpeed * Time.deltaTime;

            // Shoot if not too far away from player
            if (Vector3.Distance(transform.position, player.transform.position) <= maxDist)
            {
                // Pause between shots
                if (Time.time - lastShot >= shootingDelta)
                {
                    lastShot = Time.time;
                    Shoot();
                }
            }
        }
    }

    private void Shoot()
    {
        if (projectilePrefab)
        {
            GameObject projectile = Instantiate(projectilePrefab) as GameObject;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();

            projectile.transform.Translate(transform.position + transform.forward);
            rb.velocity = transform.forward * shootingSpeed;
        }
    }
}
