﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int remainingLives = 3;
    public GameObject[] lifeIcons;
    public AudioMixerGroup gameplaySounds;
    public GameObject damageFlash;

    public GameObject playerPrefab;
    public GameObject spawnPoint;

    [SerializeField]
    public Transform gameOverMenu;
    private bool isPlayerShooting = false;
    private float forwardVelocity;

    public void SetSpawnPoint(GameObject spawnPoint)
    {
        this.spawnPoint = spawnPoint;
    }

    public IEnumerator RespawnPlayer()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject player = Instantiate(playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
        ThirdPersonPlayerController playerController = player.GetComponent<ThirdPersonPlayerController>();
        playerController.forwardVelocity = forwardVelocity;
        if (isPlayerShooting)
        {
            playerController.EnableShooting();
        }
        else
        {
            playerController.DisableShooting();
        }
        yield return new WaitForSeconds(2f);
    }

    public IEnumerator DamageFlash()
    {
        damageFlash.transform.GetChild(0).GetComponent<Image>().color = Color.white;
        damageFlash.SetActive(true);
        damageFlash.transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(0, 0.6f, false);
        yield return new WaitForSeconds(0.61f);
        damageFlash.SetActive(false);
    }

    public void KillPlayer(GameObject player)
    {
        ThirdPersonPlayerController playerController = player.GetComponent<ThirdPersonPlayerController>();
        isPlayerShooting = playerController.IsPlayerShooting();
        forwardVelocity = playerController.forwardVelocity;

        remainingLives--;
        lifeIcons[remainingLives].SetActive(false);
    }

    public void PlayerDead(GameObject player)
    {
        Destroy(player);
        if (remainingLives > 0)
        {
            StartCoroutine(RespawnPlayer());
        }
        else
        {
            gameplaySounds.audioMixer.SetFloat("volume", 0);
            gameOverMenu.gameObject.SetActive(true);
        }
    }

    public void ReplayGame()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadSceneAsync(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
