﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCardiogramSound : MonoBehaviour
{
    public AudioSource music;
    public float maxDelay;
    private float delay = 0.0f;

    void Update()
    {
        if (!music.isPlaying && delay >= maxDelay)
        {
            music.Play();
            delay = 0f;
        }
        else if (!music.isPlaying)
        {
            delay += Time.deltaTime;
        }
    }
}
