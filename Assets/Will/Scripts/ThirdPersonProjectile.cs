﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonProjectile : MonoBehaviour
{
    private Rigidbody rb;
    public float speed = 20.0f;
    public float timeToLive = 5f;
    public int damage = 10;
    public LayerMask castMask;
    private Vector3 oldPos;
    private RaycastHit hitInfo;

    private float lifetime;

    private void OnEnable()
    {
        GetComponent<Renderer>().enabled = true;
        lifetime = 0.0f;
    }

    void Start()
    {
        lifetime = 0.0f;
        oldPos = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        CollisionDetection(other);
    }

    private IEnumerator Burst()
    {
        GetComponent<Renderer>().enabled = false;
        transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(transform.GetChild(0).GetComponent<ParticleSystem>().main.duration);
        gameObject.SetActive(false);
    }

    void Update()
    {
        //rb.velocity = transform.forward * speed;
        lifetime += Time.deltaTime;


        //Vector3 heading = transform.position - oldPos;
        //float distance = heading.magnitude;
        //Vector3 direction = heading / distance;
        //Ray ray = new Ray(oldPos, direction);
        //Debug.DrawRay(oldPos, direction, Color.yellow);
        //if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, castMask, QueryTriggerInteraction.Collide))
        //{
        //    var hitObject = hitInfo.collider;
        //    CollisionDetection(hitObject);
        //}
        //oldPos = transform.position;

        if (lifetime > timeToLive)
        {
            gameObject.SetActive(false);
            lifetime = 0.0f;
        }
    }

    private void CollisionDetection(Collider other)
    {
        var rangedEnemy = other.gameObject.GetComponentInParent<RangedEnemyController>();
        var meleeEnemy = other.gameObject.GetComponentInParent<MeleeEnemyController>();
        var staticEnemy = other.gameObject.GetComponentInParent<StaticEnemyController>();
        var bossPumpkin = other.gameObject.GetComponentInParent<BossPumpkingController>();

        if (rangedEnemy != null)
        {
            rangedEnemy.TakeDamage(damage);
            StartCoroutine(Burst());
        }
        else if (meleeEnemy != null)
        {
            meleeEnemy.TakeDamage(damage);
            StartCoroutine(Burst());
        }
        else if (staticEnemy != null)
        {
            staticEnemy.TakeDamage(damage);
            StartCoroutine(Burst());
        }
        else if (bossPumpkin != null)
        {
            bossPumpkin.TakeDamage(damage);
            StartCoroutine(Burst());
        }
    }

    public void SetOldPosition(Vector3 pos)
    {
        oldPos = pos;
    }
}
