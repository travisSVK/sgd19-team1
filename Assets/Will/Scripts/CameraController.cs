﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;        //Public variable to store a reference to the player game object
    private float offset;            //Private variable to store the offset distance between the player and camera

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position.z - player.transform.position.z;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + offset);
        }
        else
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }
}
